const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const usersRoutes = require('./routes/users')

dotenv.config();

const application = express();
const db = process.env.DB;
const port = process.env.PORT;

application.use(express.json());
application.use(cors());
application.use('/users', usersRoutes);

mongoose.connect(db);
mongoose.connection.once('open', () => {
	console.log("QMarketz DB is now connected");
});

application.listen(port, () => {
	console.log(`Port ${port} is now up and running`);
});

application.get('/', (request, response) => {
	response.send('Welcome QMarketz!')
})