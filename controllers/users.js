const User = require('../models/User');
const dotenv = require('dotenv');
const mongoose = require('mongoose');

dotenv.config();

module.exports.getUsers = () => {
	return User.find({}).then((users) => {
		if(users.length > 0){
			return {message: `Record(s) found: ${users.length}`,
					result: users}
		}
		else{
			return {message: `No users found`}
		}
	}).catch((error) => {
		return error.message;
	})
};

module.exports.createUsers = (request) => {
	return User.create({first_name: request.fname, 
		middle_name: request.mname, 
		last_name: request.lname, 
		address: request.address, 
		age: request.age}).then((createUser) => {
		return {message: `User created!`,
				details: createUser}
	}).catch((error) => {
		return error.message;
	})
};

module.exports.updateUsers = (request) => {
	return User.updateOne({_id: request.id}, {
		$set: {
			first_name: request.fname,
			middle_name: request.mname,
			last_name: request.lname,
			address: request.address,
			age: request.age
		}
	}).then((updateUser) => {
		if(updateUser.modifiedCount === 1){
			return {message: `User info updated!`}
		}
		else{
			return {message: `User info failed to update!`}	
		}
	}).catch((error) => {
		return error.message;
	})
};

module.exports.deleteUsers = (request) => {
	return User.deleteOne({_id: request.id}).then((deleteUser) => {
		return {message: `User id: ${request.id} removed!`}
	}).catch((error) => {
		return error.message;
	})
};