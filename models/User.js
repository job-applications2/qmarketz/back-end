const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
	first_name: {
		type: String,
		required: [true, 'First name required']
	},
	middle_name: {
		type: String,
		required: [true, 'Middle name required']
	},
	last_name: {
		type: String,
		required: [true, 'Last name required']
	},
	address: {
		type: String,
		required: [true, 'Address required']
	},
	age: {
		type: Number,
		required: [true, 'Age required']
	}
})

module.exports = mongoose.model('User', userSchema);