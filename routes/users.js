const express = require('express');
const usersController = require('../controllers/users');
const router = express.Router();

router.get('/all', (request, response) => {
	usersController.getUsers().then((allUser) => {
		response.send(allUser);
	})
});

router.post('/create', (request, response) => {
	usersController.createUsers(request.body).then((createUser) => {
		response.send(createUser);
	})
});

router.put('/update', (request, response) => {
	usersController.updateUsers(request.body).then((updateUser) => {
		response.send(updateUser);
	})
});

router.delete('/delete', (request, response) => {
	usersController.deleteUsers(request.body).then((deleteUser) => {
		response.send(deleteUser);
	})
})

module.exports = router;